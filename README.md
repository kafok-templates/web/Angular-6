# Plantilla Angular 6
Esta plantilla es un proyecto pre-configurado en Angular 6 para trabajar en VS Code. Cada vez que se realice un cambio en la base se actulizará la rama `master`. Cada rama actualizá su `README.md` para explicar que modificaciones sobre la plantilla inicial tiene dicha rama. Si se modifica la rama `master` se hará una pull-request a todas las demás automáticamente.

# ¿Cómo usar esta plantilla?
Para usar esta plantilla haga lo siguiente:
1. Clonese este repositorio, donde *NOMBRE_PROYECTO* será el nombre que le desee dar al proyecto:
```
git clone https://gitlab.com/kafok-templates/web/Angular-6.git NOMBRE_PROYECTO
cd NOMBRE_PROYECTO
```

2. Ahora borre la carpeta *NOMBRE_PROYECTO/.git* para reiniciar su repositorio.
3. Cree su repositorio de nuevo, esta vez limpio:
```
git init
git add .
git commit -m "Primer commit"
```

4. En caso de que quiera añadir cualquier modificación que se encuentre en otra rama, antes del paso dos haremos:
```
git pull origin NOMBRE_DE_LA_RAMA
```

5. Elimine los archivos `README.md`, `.gitlab-ci.yml` y `LICENCE.md` o sustitúyalos por los suyos propios.
6. Cambiar en los archivos `package.json`, `src/index.html` y `angular.json` la palabra *NOMBRE_PROYECTO* por el nombre de su proyecto (recuerde seguir la convención de nombres para cada archivo).
7. Instale las dependencias de node:
```
npm install
```

# Ramas de este repositorio
## master
Es la configuración de Angular con las librerías por defecto. Cualquier cambio en esta rama afectará a todas la demás y puede deberse por cambios de la configuración base, arreglos de esta o actualizaciones de librerías. 

## lib/**
Todas las ramas que empiecen por este nombre son pre-configuraciones de una librería concreta.

### angular-material
Esta rama trae configurado Angular Material y Material Icons.

## electronjs
Integrado Angular con el framework de aplicaciones híbridas de escritorio ElectronJS.
